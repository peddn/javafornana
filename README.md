# javafornana
## 1. Erschaffe zwei Blasen
In diesem Projekt exsitiert eine **Klasse**, welche Blasen **beschreibt**. In dieser Klasse steht, welche **Eigenschaften** alle Blasen gemein haben und was jede Blase tun kann. Alles was Blasen tun können nennt man **Methoden**. Diese Klasse kann auch neue Blasen erschaffen. Die Methode, in der dies geschieht, hat einen speziellen Namen. Diese Methode heißt **Konstruktor**. Das Erschaffen einer neuen Blase nennt man **Initilaisieren**.

### **Aufgabe**
- Deklariere jetzt zwei Blasen. Nenne diese Blasen `bubble1` und `bubble2`.

- Benutze anschließend den Konstruktor der Blasen-Klasse um diese zwei neuen Blasen zu initialisieren.

- Befiehl nun den Blasen, dass sie sich zeichnen und snschließend updaten sollen.

- Starte das Programm mit

  ```gradle run --args='Arrays1'```

### *Hinweis*
- Der Bubble Konstruktor erwartet ein PApplet Objekt. Dieses PApplet Object ist das "Fenster" auf dem gezeichnet wird. Die Klasse `Arrays1` erbt von PApplet, also kannst du das Arrays1 Objekt selbst mit `this` an den Bubble Konstruktor übergeben.

- Wie du den Blasen befehlen kannst, wie sie sich zeichnen und updaten sollen, erfährst du durch einen Blick in die Blasen-Klasse `Bubble.java`.


## 2. Deklariere ein Array aus Blasen

## 3. Initialisiere das Array mit Platz für 