package net.peddn.javafornana;

import processing.core.*;

public class Arrays1 extends PApplet {

    // Hier deklariert man alles was man in seiner Welt haben will.
    Bubble bubble1;
    Bubble bubble2;

    // Diese funktion wir als erstes aufgerufen.
    // Hier kann man die Fenstergroesse und das Smoothnes-Level festlegen.
    public void settings() {
        size(800, 600);
        smooth(2);
    }

    // Diese Funktion wird als naechstes aufgerufen.
    // Hier instanziert man alles was man in seiner Welt haben will.
    public void setup() {
        bubble1 = new Bubble(this);
        bubble2 = new Bubble(this);
    }

    // Diese Funktion wird nach setup() 60 mal pro Sekunde aufgerufen.
    // Hier koennen sich zum Beispiel Objekte zeichen.
    public void draw() {
        background(100);
        bubble1.display();
        bubble2.display();
        bubble1.update();
        bubble2.update();
    }

}