package net.peddn.javafornana;

import processing.core.PApplet;

public class Run {

  public static void main(String[] args) {
    String sketch = args[0];
    PApplet.main("net.peddn.javafornana." + sketch);
  }

}