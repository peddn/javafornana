package net.peddn.javafornana;

import processing.core.PApplet;

public class Bubble {
    private PApplet p; // The parent PApplet that we will render ourselves onto

    private float x;
    private float y;
    private float r;
    private int c;

    private int counter;
    
    public Bubble(PApplet p) {
        this.p = p;
        this.r = Helper.randomBetween(10, 50);
        this.x = Helper.randomBetween(0, p.width);
        this.y = p.height - this.r;
        
        this.c = Helper.randomBetween(100, 255);

    }

    public void display() {
        this.p.fill(this.c);
        this.p.ellipse(this.x, this.y, this.r*2, this.r*2);
    }

    public void update() {
        //this.y--;
        counter++;
        if(counter > 59) {
            System.out.println("1 sec");
            counter = 0;
        }

    }
}