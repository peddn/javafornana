package net.peddn.javafornana;

import java.util.Random;

public class Helper {

    private static final Random r = new Random();

    public static int randomBetween(int low, int high) {
        return Helper.r.nextInt(high-low) + low;
    }
}